
//document.getAttribute('href') + window.location.href;

const media = document.querySelector('video');
const controls = document.querySelector('.controls');

const play = document.querySelector('.play');
const stop = document.querySelector('.stop');

const timerWrapper = document.querySelector('.timer');
const timer = document.querySelector('.timer span');
const timerBar = document.querySelector('.timer div');


media.removeAttribute('controls');
controls.style.visibility = 'visible';

play.addEventListener('click', playPausedMedia);

function playPausedMedia(){
    if(media.paused){
        play.setAttribute('data-icon', 'fa-play-circle');
        media.play();
    }else{
        play.setAttribute('data-icon', 'fa-pause');
        media.paused();
    }
}

stop.addEventListener('click', stopMedia);

function stopMedia(){
    media.pause();
    media.currentTime = 0;
    play.setAttribute('data-icon', 'fa-play-circle');
}

media.addEventListener('timeupdate', setTime);

function setTime(){
    
    let minutes = Math.floor(media.currentTime / 60);
    let seconds = Math.floor(media.currentTime - minutes * 60);
    let minuteValue;
    let secondValue;
    
    if(minutes < 10){
        minuteValue = '0' + minutes;
    }else{
        minuteValue = minutes;
    }
    
    if(seconds < 10){
        secondValue = '0' + seconds;
    }else{
        secondValue = seconds;
    }
    
    let mediaTime = minuteValue + ':' + secondValue;
    timer.textContent = mediaTime;
    
    let barLength = timerWrapper.clientWidth * (media.currentTime / media.duation);
    timerBar.style.width = barLength + 'px';
}